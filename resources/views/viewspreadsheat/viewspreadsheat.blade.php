<!DOCTYPE HTML>
<html>
	<head>
		<title>Leroy</title>
	

	    <!-- Bootstrap -->
	    <link href="<?php echo URL::to('/css/animate.css') ?> " rel="stylesheet">
	    <link href="<?php echo URL::to('/css/bootstrap.min.css') ?>"  rel="stylesheet">
	    <link href="<?php echo URL::to('/css/font-awesome.min.css') ?>" rel="stylesheet">
	    <link href="<?php echo URL::to('/css/graph.css') ?>" rel="stylesheet">
	    <link href="<?php echo URL::to('/css/icon-font.min.css') ?>" rel="stylesheet">
	    <link href="<?php echo URL::to('/css/style.css') ?>" rel="stylesheet">
	    <link href="<?php echo URL::to('/css/custom.css') ?>" rel="stylesheet">
	    <link href="<?php echo URL::to('graph/css/highcharts.css') ?>" rel="stylesheet">

    

	    <!--jAlert CSS-->
	    <link href="<?php echo URL::to('/css/jAlert.css') ?>" rel="stylesheet">
	    
	    <!--CSS select2-->
	    <link href="<?php echo URL::to('/css/select2.css') ?>" rel="stylesheet">	    

	    <!--liveicons-->
	    <link href="<?php echo URL::to('/liveicons/css/styleliveicon.css') ?>" rel="stylesheet">
	    <link href="<?php echo URL::to('/liveicons/css/customlivicons.css') ?>" rel="stylesheet">
	    <link href="<?php echo URL::to('/liveicons/css/dialog.css') ?>" rel="stylesheet">
	    <link href="<?php echo URL::to('/liveicons/css/jquery.minicolors.css') ?>" rel="stylesheet">
	    <link href="<?php echo URL::to('/liveicons/css/livicons-help.css') ?>" rel="stylesheet">
	    <link href="<?php echo URL::to('/liveicons/css/livicons-shortcodes.css') ?>" rel="stylesheet">
	    <link href="<?php echo URL::to('/liveicons/css/prettify.css') ?>" rel="stylesheet">


	    <!--modal-->
	    <link href="<?php echo URL::to('/modal/css/component.css') ?>" rel="stylesheet">
	    <link href="<?php echo URL::to('/modal/css/default.css') ?>" rel="stylesheet">
	    

	    <!--checkbox bootstrap-->
		<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
		<link href="<?php echo URL::to('/css/filter.css') ?>" rel="stylesheet">

	    <!--datepicker-->
	    <link href="<?php echo URL::to('/css/datepicker/bootstrap-datepicker3.css') ?>" rel="stylesheet">




	    
  		<script src="<?php echo URL::to('/js/jquery-1.10.2.min.js') ?>"  ></script>
		<script src="<?php echo URL::to('/js/classie.js') ?>"  ></script>
	    <script src="<?php echo URL::to('/js/Chart.js') ?>"  ></script>
	    <script src="<?php echo URL::to('/js/jquery.flexisel.js') ?>"  ></script>
	    <script src="<?php echo URL::to('/js/jquery.flot.min.js') ?>"  ></script>
	    <script src="<?php echo URL::to('/js/skycons.js') ?>"  ></script>
	    <script src="<?php echo URL::to('/js/uisearch.js') ?>"  ></script>
	    <script src="<?php echo URL::to('/js/wow.min.js') ?>"  ></script>
	    <script src="<?php echo URL::to('/js/jquery.twbsPagination.min.js') ?>"  ></script>


	   	
	   	<!--jAlertjs-->
	   	<script src="<?php echo URL::to('/js/jAlert-functions.min.js') ?>"  ></script>
	    <script src="<?php echo URL::to('/js/jAlert.min.js') ?>"  ></script>
	    <script src="<?php echo URL::to('/js/jquery-ui/jquery-ui.js') ?>"  ></script>
</head>


<body>

	<input type="hidden" name="base_url" id="base_url" value="{{ URL::to('/delFile') }}"  >

	<div class="alert alert-success">{{$message}}"</div>

    <table  class="table table-bordered table-condensed">		
		 <div class="hidden div-message" >{{$sum=0}}</div>

		 @while($total>$sum)

		 	
		   @if($sum>0) 
		 	<tr>	
		 		<td>{{$resp[$sum][0]}}</td>
		 		<td>{{$resp[$sum][1]}}</td>
		 		<td>{{$resp[$sum][2]}}</td>
		 		<td>{{$resp[$sum][3]}}</td>
		 		<td>{{$resp[$sum][4]}}</td>

		 		@if($sum==1)
		 		 <td>Ações</td>
		 		@else
		 		  <td>

                      		   <div >
			                       <a href='#' class="md-trigger edit_prod" data-value="{{$resp[$sum][0]}}" data-modal="modal-1"> 
				                       <span>Editar<span>
		                           </a>
		                        </div>

		                        <div >
			                       <a href="#" class="md-trigger del_prod" data-value="{{$resp[$sum][0]}}"  > 
				                       
				                        <span>Excluir<span>
		                           </a>
		                        </div>
		                      <div class="clearfix"></div> 		  	

		 		  </td>
		 		@endif
		  	
		  	</tr>
		  @endif	

		  	<div class="hidden">{{$sum++}}</div>
		 @endwhile
    </table>	



</body>		

<script >

 $('document').ready(function(){

 	$('.del_prod').on('click',function(){
 		//id modal  to delete
 		var id               = $(this).data('value');

     	var btn  = $(this),
    	    show = btn.data('show'),
    	    hide = btn.data('hide');

							
		            //=============confirm yes no========================//
		            $.jAlert({

		              'title':'Tipo de Arquivo',
		              'type':'confirm',
		              'content':'Deseja Excluir?',
		                  'theme': 'blue',
		                  'showAnimation' : show,
		                  'hideAnimation' : hide,
		                  'confirmBtnText': "Sim",
		                  'denyBtnText': 'Não',
		              'onConfirm': function() {

		                    //==============DEL CARGA=======================//


				         
				         var form_token_name  = $("#form_token_name").val();
				         var form_token_hash  = $("#form_token_hash").val();


	
				        $('.div-message').append('<form class="hidden" name="formdelete" id="formdelete"><input type="text" name="id" value="'+id+'" id="id" ><input type="hidden" name="_token" value="{{{ csrf_token() }}}" > </form>')

				     	var url = $("#base_url").val();

				        $("#formdelete").attr("method", "POST");
				        $('#formdelete').attr('action', url);
				        $("#formdelete").submit();



		              },
		            });


                    
                });
                return false;       


 	});		

 		
 	 


 

 </script>		