<?php

namespace App\RepositoryRepository;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\RepositoryRepositories\ComunicationRepository;
use App\RepositoryModels\Comunicacao;
use App\RepositoryValidators\ComunicacaoValidator;

/**
 * Class ComunicacaoRepositoryEloquent.
 *
 * @package namespace App\RepositoryRepository;
 */
class ComunicationRepositoryEloquent extends BaseRepository implements ComunicationRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Comunicacao::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
