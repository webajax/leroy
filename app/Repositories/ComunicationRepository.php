<?php
// app/Repositories/PostRepository.php
namespace App\Repositories;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;



class ComunicationRepository 
{
        
        public function __construct()
        {
                
        }
        
        public function getFile()
        {
			$client = new Client();
			$res = $client->request('GET', 'http://localhost/leroy/public/api/getjson', [
			   'auth' => ['user', 'pass']
			]);

			if($res->getStatusCode()==200)
				return response($res->getBody(),200);
			else
				return response("Houve um erro ao ler json",500);

        }

        public function delFile($response,$id){

			$client = new Client();
			$res = $client->delete('DELETE', 'http://localhost/leroy/public/api/delfile/$response/$id');


			if($res->getStatusCode()==200)
				return response($res->getBody(),200);
			else
				return response("Houve um erro ao excluir registro",500);        	


        }
        
}
?>
