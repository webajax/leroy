<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Exports\UsersExport;
use Importer;


class SavexlsCommand extends Command 
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    protected $file;
    protected $excel;

    public function __construct($file)
    {
        
       $this->file  = $file;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() #queue 
    {

       #upload
       $response =  $this->uploadFile($this->file);

       if($response->getstatusCode()==500){
        echo $response->getContent();
        exit;
       }

         $data[] = array(
          'message'    => $response->getContent(),
          'status'     => $response->getStatusCode(),
          'type'       => 0,
          'exception'  => $response->exception
         );

         #convert xlsx json txt
         $resp = $this->convertJsonFile($this->file);

         $data[] = array(
          'message'    => $resp->getContent(),
          'status'     => $response->getStatusCode(),
          'type'       => 1,
          'exception'  => $response->exception
         );
       
 
 
       return $data; 
    }

    public function uploadFile($file){

      try{  

        $path = "./images";

              

        #--------------------------------------------------GET original name
        $originalname = $file->getClientOriginalName();

        # Display File Extension
        if($file->getClientOriginalExtension()=="xlsx"){
         
              # if dir exist
              # if not exist create dir
              if (!is_dir($path))
                 mkdir($path, 0777, $recursive = true);
              
              #upload file
              $response = $file->move($path,$file->getClientOriginalName());

              if(http_response_code()==200)
                return response('Upload efetuado com sucesso',200)->header('Content-Type','text/plain');
              else
                return response(http_response_code());              


        }else{
         
            return response("Formato de arquivo inválido",500)->header('Content-Type','text/plain');
           
        }    

     }catch(Exception $e){

        return response("Houve um erro ao fazer o upload do arquivo!",500);
      

     }   

         
    }

    public function convertJsonFile($file){
     
      try{   

        #path file
        $path = "./images/".$file->getClientOriginalName();
        
        #open xls
        $excel = Importer::make('Excel');
        $excel->load($path);
        $collection = $excel->getCollection(); #get cells and transform array 
        $collec = json_encode($collection);#convert array and json

        #open or create file option a
        unlink("./images/json.txt"); #delete file
        $fp = fopen("./images/json.txt", "a");
        $white = fwrite($fp, $collec);
        fclose($fp);

        if(count($collection)>1)
             return response('Json criado com sucesso!',200);
        else
            return response('Arquivo com erros ou sem registro!',500);
           

     
    }catch(Exception $e){

        return response("Erro ao ler arquivo!",500);

    }
   } 

}
