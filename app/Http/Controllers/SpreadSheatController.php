<?php


namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Console\Commands\SavexlsCommand;
use Illuminate\Foundation\Bus\DispatchesCommands;
use App\Repositories\ComunicationRepository;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Collection;
use View;
use Exporter;
use Excel;




class SpreadSheatController extends Controller{
    
	protected $repository;
	protected $file;

    public function __construct(ComunicationRepository $repository){
       
    	$this->repository = $repository;
    	

    }


    public function  index(){


		return View::make('spreadsheat.spreadsheat');

    }
    
    public function uploadFile(Request $request){

    	$this->file = $request->file("cad_carga_file");

    	#upload file and read file dispatchNow sincronous queue
    	$data = $this->dispatchNow($response = new SavexlsCommand($this->file));



    	if($data[0]["status"]==200 && $data[1]["status"]==200){

    		#access end point
    		$response = $this->repository->getFile($this->file);
            
            #convert json in array
            $data     = json_decode($response->getContent());
            
            #get total itens array
            $total     = count($data);
            $sum       = 0;
            $dataarray = array();

    


            return View::make('viewspreadsheat.viewspreadsheat')
            ->with('resp', $data)
            ->with('total', $total)
            ->with('message', "Arquivos carregados com sucesso!");

    	}


    }


    public function delFile(Request $request){


        $id = $request->input('id');

        #access end point
        $resp = $this->repository->getFile($this->file);

        #convert json in array
        $data     = json_decode($resp->getContent());

        $total     = count($data);

        $sum=2;
        
        while($total>$sum){#rdelete item array
            
            if($sum>1){           

              if($id == $data[$sum][0] ) {
                unset($data[$sum]); 
                break;
              }

            }

            $sum++;
        }

        #get total itens array
        $total     = count($data);
        $sum       = 0;

        $data = array_values($data);


        return View::make('viewspreadsheat.viewspreadsheat')
            ->with('resp', $data)
            ->with('total', $total)
            ->with('message', "Sucesso!Excluído!");;




    }
}
