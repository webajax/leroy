<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $path = "./images/json.txt";

        $pointer = fopen ($path,"r");

        $line="";
         #--------------------------------------------------read file txt
        while (!feof ($pointer)) 
            $line = $line . fgets($pointer,4096);#ready line        
          
        

        return parent::toArray($line);
    }
}
