<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|{
*/

Route::get('index', 'SpreadSheatController@index');#call service instancy
Route::post('uploadFile', 'SpreadSheatController@uploadFile');#call service instancy
Route::post('delFile', 'SpreadSheatController@delFile');#call service instancy

